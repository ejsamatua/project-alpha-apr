from django.db import models
from django.contrib.auth.models import User


# This creates a 'Project' in our projects Django app


class Project(models.Model):
    name = models.CharField(max_length=200)

    description = models.TextField()

    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name
