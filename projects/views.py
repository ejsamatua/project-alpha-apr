from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm

#####################################################################
# This function creates a list view to see all projects for the project model.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"project_list": projects}

    return render(request, "projects/list.html", context)


#####################################################################
# This allows people to see the details about a project


@login_required
def show_project(request, id):
    projects = Project.objects.get(id=id)
    project_details = Task.objects.filter(project=id)
    context = {
        "projects": projects,
        "project_details": project_details,
    }

    return render(request, "projects/detail.html", context)


#####################################################################
# This allows people to create a new project


@login_required
def create_project(request):
    form = ProjectForm(request.POST)
    if request.method == "POST":
        form = ProjectForm(request.POST)
    if form.is_valid():
        project = form.save(False)
        project.owner = request.user
        project.save()
        return redirect("list_projects")

    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
