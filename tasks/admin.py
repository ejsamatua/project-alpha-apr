from django.contrib import admin
from tasks.models import Task


# Register your models here.

# Admin view to see our Task inside localhost:8000/admin


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
        "id",
    ]
