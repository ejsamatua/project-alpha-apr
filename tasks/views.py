from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


#####################################################################
# This function allows a logged in person to create a new Task for our Project


@login_required
def create_task(request):
    form = TaskForm(request.POST)
    if request.method == "POST":
        form = TaskForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect("list_projects")

    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


#####################################################################
# This function allows a logged person to see the tasks assigned to them


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
    }
    return render(request, "tasks/mine.html", context)
